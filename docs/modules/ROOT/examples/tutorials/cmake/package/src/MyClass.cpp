
#include "package/MyClass.h"

namespace daq::MyPackage {

    MyClass::MyClass(int initial)
        : m_value(initial)
    {}

    MyClass::~MyClass()
    {
    }

    int MyClass::get() const
    {
        return m_value;
    }

    void MyClass::set(int new_value)
    {
        m_value = new_value;
    }

}
