#include "package/MyClass.h"
#include <iostream>

int main()
{
  daq::MyPackage::MyClass c(10);

  if (c.get() != 10) {
      std::cerr << "MyClass: init failed" << std::cerr;
      exit(1);
  }

  c.set(20);
  if (c.get() != 20) {
      std::cerr << "MyClass: set/get failed" << std::cerr;
      exit(1);
  }

  return 0;

}
