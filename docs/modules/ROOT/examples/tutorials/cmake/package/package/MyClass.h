// this is -*- c++ -*-
#ifndef MYPACKAGE_HEADER_
#define MYPACKAGE_HEADER_

namespace daq {
    namespace MyPackage {

        class MyClass {
        public:
            explicit MyClass(int initial = 0);
            ~MyClass();

            int get() const;
            void set(int new_value);

        private:
            int m_value;
        };
    }
}

#endif // MYPACKAGE_HEADER_
